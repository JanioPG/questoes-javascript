var products_list = [
    {
      "id": "35",
      "name": "Miniatura Homem de Ferro",
      "price": "R$ 50.00"
    },
    {
      "id": "30",
      "name": "Miniatura Magneto",
      "price": "R$ 57.00"
    },{
      "id": "25",
      "name": "Miniatura Super Homem",
      "price": "R$ 37.00"
    },{
      "id": "71",
      "name": "Miniatura Bernard",
      "price": "R$ 71.00"
    },{
      "id": "56",
      "name": "Miniatura Batman",
      "price": "R$ 23.00"
    },
    {
      "id": "56",
      "name": "Miniatura Darth Vader",
      "price": "R$ 89.00"
    }];


function tratarString(array) {
    let novo = array.map(item => {
        item.price = item.price.replace('R$ ', '');
        item['item_price'] = parseInt(item.price);
        delete item.price;
        return item;
    });
    return novo;
};

let new_products_list = tratarString(products_list);
console.log(new_products_list);
